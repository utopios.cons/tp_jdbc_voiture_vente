package org.example.dao;

import org.example.entity.Person;

import java.util.ArrayList;

public interface PersonDao {


    void addPerson(Person p);
    ArrayList<Person> showPerson();

    void deletePerson(int id);

    Person getPersonByID(int id);

    void updatePerson(int id, String nom, int age);

    void getCarByIdPerson(int id);

}
