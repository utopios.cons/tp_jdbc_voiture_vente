/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.entity;


public class Vente {

    private Person p;
    private Car c;
    private String date_vente;

    public Vente(Person p, Car c) {
        this.p = p;
        this.c = c;
        this.date_vente = java.time.LocalDate.now().toString();
    }

    public Person getP() {
        return p;
    }

    public void setP(Person p) {
        this.p = p;
    }

    public Car getC() {
        return c;
    }

    public void setC(Car c) {
        this.c = c;
    }

    public String getDate_vente() {
        return date_vente;
    }

    public void setDate_vente(String date_vente) {
        this.date_vente = date_vente;
    }

    @Override
    public String toString() {
        return "Vente{" + "p=" + p + ", c=" + c + ", date_vente=" + date_vente + '}';
    }

}
