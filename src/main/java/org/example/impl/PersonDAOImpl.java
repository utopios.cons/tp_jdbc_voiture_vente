/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.impl;


import org.example.dao.PersonDao;
import org.example.entity.Person;
import org.example.utils.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class PersonDAOImpl implements PersonDao {


    private java.sql.Connection con = null;


    public PersonDAOImpl() {
        con = Connection.getConnection();
    }

    public void addPerson(Person p) {

        String sql = "INSERT INTO person (id_person, prenom, age, nom) VALUES (?, ?, ?, ?)";

        try {
            con = Connection.getConnection();

            PreparedStatement stmt = con.prepareStatement(sql);


            stmt.setInt(1, p.getId());


            stmt.setString(2, p.getPrenom());
            stmt.setInt(3, p.getAge());
            stmt.setString(4, p.getName());


            stmt.execute();
            System.out.println("\nPersonne ajoutée dans la base de données\n");


        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }

    public ArrayList<Person> showPerson() {
        ArrayList<Person> liste = new ArrayList<>();
        String sql = "SELECT * FROM person";

        try {
            con = Connection.getConnection();

            PreparedStatement stmt = con.prepareStatement(sql);


            ResultSet rs = stmt.executeQuery();


            while (rs.next()) {


                Person p = new Person();
                ;
                p.setId(rs.getInt("id_person"));
                p.setPrenom(rs.getString("prenom"));
                p.setAge(rs.getInt("age"));
                p.setName(rs.getString("nom"));

                liste.add(p);

            }

            rs.close();

            return liste;

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return null;
        } finally {
            Connection.closeConnection(con);
        }
    }

    public void deletePerson(int id) {


        String sql0 = "DELETE FROM vente WHERE id_person_fk = ?";
        String sql = "DELETE FROM person WHERE id_person = ?";

        try {
            con = Connection.getConnection();


            PreparedStatement stmt1 = con.prepareStatement(sql0);

            stmt1.setInt(1, id);
            stmt1.executeUpdate();


            PreparedStatement stmt = con.prepareStatement(sql);

            stmt.setInt(1, id);
            stmt.executeUpdate();
            System.out.println("\nPersonne supprimée de la base de données\n");
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }

    public Person getPersonByID(int id) {

        Person p = new Person();
        String sql = "SELECT * FROM person WHERE id = ?";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                p.setId(rs.getInt("id_person"));
                p.setPrenom(rs.getString("prenom"));
                p.setAge(rs.getInt("age"));
                p.setName(rs.getString("nom"));
            }
            return p;

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return null;
        } finally {
            Connection.closeConnection(con);
        }
    }

    public void updatePerson(int cpf, String nom, int age) {

        String sql = "UPDATE person SET nom = ?, age = ? WHERE id_person = ?";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, nom);
            stmt.setInt(2, age);
            stmt.setInt(3, cpf);
            stmt.executeUpdate();
            System.out.println("\nPersonne modifiée dans la base de données\n");
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }

    public void getCarByIdPerson(int id) {

        String sql = "SELECT v.id_vente, p.id_person || ' - ' || p.nom as ACHETEUR, c.id_car || ' - ' || c.nom as car, c.prix, v.date_vente  FROM vente as v, person as p, car as c where p.id_person = ? and c.id_car = v.id_car_fk";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            System.out.println("\nToutes les voitures");

            while (rs.next()) {
                System.out.println("Car: " + rs.getString("car"));
            }

            System.out.print("\n");
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }

}
