package org.example;



import org.example.impl.CarDAOImpl;
import org.example.impl.PersonDAOImpl;
import org.example.impl.VenteDAOImpl;
import org.example.entity.Car;
import org.example.entity.Person;
import org.example.entity.Vente;

import java.util.Scanner;


public class Main {



    public static void main(String[] args) {

        Main m = new Main();
        m.menu();
    }

    public Scanner getScanner() {
        return new Scanner(System.in);
    }

    public void menu() {

        System.out.println("1) Enregistrer la voiture");
        System.out.println("2) Lister toutes les voitures");
        System.out.println("3) Supprimer la voiture");
        System.out.println("4) Modifier la voiture");
        System.out.println("5) Créer une personne");
        System.out.println("6) Lister toutes les personnes");
        System.out.println("7) Supprimer une personne");
        System.out.println("8) Modifier une Personne");
        System.out.println("9) Créer une vente");
        System.out.println("10) Affiche vente");
        System.out.println("11) Afficher la ou les voiture(s) achetée(s) par une personne");
        System.out.println("12) Quitter");
        System.out.print("Saisissez une Option: ");
        int op = getScanner().nextInt();

        switch (op) {

            case 1:
                addCar();
            case 2:
                listeCar();
            case 3:
                deleteCar();
            case 4:
                updateCar();
            case 5:
                addPerson();
            case 6:
                listePerson();
            case 7:
                deletePerson();
            case 8:
                updatePerson();
            case 9:
                creerVente();
            case 10:
                afficheVente();
            case 11:
                listePersonCar();
            case 12:
                System.exit(0);
            default:
                System.out.println("Saisissez une option !");
                menu();
        }
    }

    private void addCar() {


        System.out.print("Donnez moi le nom du véhicule : ");
        String nome = getScanner().nextLine();
        System.out.print("Donnez moi la marque de la voiture : ");
        String cor = getScanner().next();
        System.out.print("Donnez moi l'année du véhicule : ");
        int ano = getScanner().nextInt();
        System.out.print("Donnez moi la puissance : ");
        int potencia = getScanner().nextInt();
        System.out.print("Donnez moi le prix de la voiture  : ");
        float valor = getScanner().nextFloat();


        Car c = new Car(nome, cor, ano, potencia, valor);


        CarDAOImpl cdao = new CarDAOImpl();
        cdao.addCar(c);

        menu();
    }

    public void listeCar() {


        CarDAOImpl cdao = new CarDAOImpl();

        System.out.println("\t\n--- Liste des voitures ---\n");

        //Passando um for no arraylist que o metodo mostrar_carros retorna
        for (Car c : cdao.showCar()) {
            System.out.println("Id de la voiture : " + c.getId());
            System.out.println("Nom : " + c.getNom());
            System.out.println("Marque : " + c.getMarque());
            System.out.println("Année : " + c.getAnnee());
            System.out.println("Puissance (CV) : " + c.getPuissance());
            System.out.println("Prix de la voiture : " + c.getPrix() + "\n");
        }
        menu();
    }

    public void deleteCar() {

        CarDAOImpl cdao = new CarDAOImpl();

        System.out.print("\nDonnez moi l'id  ");
        int numero_chassi = getScanner().nextInt();

        cdao.deleteCar(numero_chassi);
        menu();

    }

    public void updateCar() {

        CarDAOImpl cdao = new CarDAOImpl();

        System.out.print("\nDonnez moi l'id de la voiture à modifier : ");
        int numero_chassi = getScanner().nextInt();

        Car c = cdao.getCarById(numero_chassi);

        System.out.println("\n\n" +
                "Modification des informations sur la voiture \n");
        System.out.println("Id: " + c.getId());
        System.out.println("Nom: " + c.getNom());
        System.out.println("Marque: " + c.getMarque());
        System.out.println("Année: " + c.getAnnee());
        System.out.println("Puissance (CV): " + c.getPuissance());
        System.out.println("Valeur de la voiture : " + c.getPrix() + "\n");

        System.out.println("Entrez les nouvelles informations: \n");

        System.out.print("Nom: ");
        String nome = getScanner().nextLine();
        System.out.print("Marque : ");
        String cor = getScanner().next();
        System.out.print("Année: ");
        int ano = getScanner().nextInt();
        System.out.print("Puissance (CV): ");
        int potencia = getScanner().nextInt();
        System.out.print("Valeur de la voiture : ");
        float valor = getScanner().nextFloat();

        cdao.updateCar(c.getId(), nome, cor, ano, potencia, valor);
        menu();
    }

    public void addPerson() {

        System.out.print("\nNom de la  Personne: ");
        String nom = getScanner().nextLine();
        System.out.print("\nprenom de la  Personne: : ");
        String prenom = getScanner().nextLine();
        System.out.print("Age de la Pessoa: ");
        int age = getScanner().nextInt();


        Person p = new Person(prenom, age, nom);


        PersonDAOImpl pdao = new PersonDAOImpl();
        pdao.addPerson(p);

        menu();
    }

    public void listePerson() {


        PersonDAOImpl pdao = new PersonDAOImpl();

        System.out.println("\t\n--- Liste des personnes ---\n");


        for (Person p : pdao.showPerson()) {
            System.out.println("Nom: " + p.getName());
            System.out.println("Id: " + p.getId());
            System.out.println("Prenom: " + p.getPrenom());
            System.out.println("age: " + p.getAge() + "\n");
        }
        menu();
    }

    public void deletePerson() {

        PersonDAOImpl pdao = new PersonDAOImpl();
        System.out.print("\nDonnez moi l'id de la personne à supprimer : ");

        int cpf = getScanner().nextInt();
        pdao.deletePerson(cpf);
        menu();
    }

    public void updatePerson() {

        PersonDAOImpl pdao = new PersonDAOImpl();

        System.out.print("\nDonnez moi l'id de la personne à modifier : ");
        int id = getScanner().nextInt();

        Person p = pdao.getPersonByID(id);

        System.out.println("\nInformation de la personne à modifier : \n");
        System.out.println("Nom: " + p.getName());
        System.out.println("Id: " + p.getId());
        System.out.println("Prenom: " + p.getId());
        System.out.println("Age: " + p.getAge() + "\n");

        System.out.println("Les données à modifier : \n");

        System.out.print("Nom: ");
        String nom = getScanner().nextLine();
        System.out.print("Age: ");
        int age = getScanner().nextInt();


        pdao.updatePerson(p.getId(), nom, age);
        menu();
    }

    public void creerVente() {

        System.out.print("\nId de l'acheteur : ");
        int id = getScanner().nextInt();
        System.out.print("Id de la voiture : ");
        int idCar = getScanner().nextInt();

        PersonDAOImpl pdao = new PersonDAOImpl();
        Person p = pdao.getPersonByID(id);

        CarDAOImpl cdao = new CarDAOImpl();
        Car c = cdao.getCarById(idCar);

        Vente v = new Vente(p, c);

        System.out.println("\nDetail de la vente: \n");
        System.out.println("Acheteur: " + p.getId() + " - " + p.getName());
        System.out.println("Car: " + c.getId() + " - " + c.getNom());
        System.out.println("Prix: " + c.getPrix());
        System.out.println("Date de la vente : " + v.getDate_vente());
        System.out.print("Fin de la vente ? 1-(Oui) 0-(Non): ");
        int reponse = getScanner().nextInt();

        if (reponse == 1) {
            VenteDAOImpl vdao = new VenteDAOImpl();
            vdao.addVente(v);
            menu();
        } else {
            System.out.print("\n");
            menu();
        }
    }

    public void afficheVente() {
        VenteDAOImpl vdao = new VenteDAOImpl();
        vdao.showVente();
        menu();
    }

    public void listePersonCar() {
        System.out.print("\nDonnez moi l'id de la personne pour obtenir la liste de ses voitures : ");
        int id = getScanner().nextInt();

        PersonDAOImpl pdao = new PersonDAOImpl();
        pdao.getCarByIdPerson(id);
        menu();
    }

}
