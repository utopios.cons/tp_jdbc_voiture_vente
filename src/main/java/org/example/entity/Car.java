/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.entity;

/**
 *
 * @author plocabral
 */
public class Car {


    private int id;
    private String nom;
    private String marque;
    private int annee;
    private int puissance;
    private double prix;

    public Car() {
    }

    public Car(int id, String nom, String marque, int annee, int puissance, double prix) {
        this.id = id;
        this.nom = nom;
        this.marque = marque;
        this.annee = annee;
        this.puissance = puissance;
        this.prix = prix;
    }
    public Car(String nom, String marque, int annee, int puissance, double prix) {
        this.nom = nom;
        this.marque = marque;
        this.annee = annee;
        this.puissance = puissance;
        this.prix = prix;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public String toString() {
        return "Car{" + "id=" + id + ", nom=" + nom + ", marque=" + marque + ", annee=" + annee + ", puissance_cv=" + puissance + ", prix=" + prix + '}';
    }

}
