package org.example.utils;

import java.sql.DriverManager;
import java.sql.SQLException;


public class Connection {


    private static final String DRIVER = "org.postgresql.Driver";

    private static final String URL = "jdbc:mysql://localhost:3306/rentcar";

    private static final String USER = "root";
    private static final String PASS = "";


    public static java.sql.Connection getConnection() {
        try {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            System.err.println("Error: " + ex);
            return null;
        }
    }

    public static void closeConnection(java.sql.Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Error: " + ex);
            }
        }
    }

}
