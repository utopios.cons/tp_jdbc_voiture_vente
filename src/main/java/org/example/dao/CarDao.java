package org.example.dao;

import org.example.entity.Car;

import java.util.ArrayList;

public interface CarDao {

    void addCar(Car c);
    ArrayList<Car> showCar();
    void deleteCar(int id);

    Car getCarById(int id);

    void updateCar(int id, String nom, String marque, int annee, int puissance, double prix);



}
