/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.entity;

/**
 *
 * @author plocabral
 */
public class Person {

    private int id;
    private String prenom;
    private int age;
    private String name;

    public Person() {
    }

    public Person(int id, String prenom, int age, String name) {
        this.id = id;
        this.prenom = prenom;
        this.age = age;
        this.name = name;
    }

    public Person(String prenom, int age, String name) {
        this.prenom = prenom;
        this.age = age;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", prenom=" + prenom + ", age=" + age + ", nom=" + name + '}';
    }

}
