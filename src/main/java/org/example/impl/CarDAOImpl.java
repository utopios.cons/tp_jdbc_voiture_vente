/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.impl;


import org.example.dao.CarDao;
import org.example.entity.Car;
import org.example.utils.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



public class CarDAOImpl implements CarDao {


    private java.sql.Connection con = null;


    public CarDAOImpl() {
        con = Connection.getConnection();
    }


    public void addCar(Car c) {


        String sql = "INSERT INTO car (nom, marque, annee, puissance, prix) VALUES (?, ?, ?, ?, ?)";

        try {
            con = Connection.getConnection();

            PreparedStatement stmt = con.prepareStatement(sql);

            stmt.setString(1, c.getNom());
            stmt.setString(2, c.getMarque());
            stmt.setInt(3, c.getAnnee());
            stmt.setInt(4, c.getPuissance());
            stmt.setDouble(5, c.getPrix());


            stmt.execute();
            System.out.println("\nVoiture ajoutée à la base de données\n");

            //Pronto aqui ele já inseriu no banco.
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }

    }


    public ArrayList<Car> showCar() {


        ArrayList<Car> listeCar = new ArrayList<>();


        String sql = "SELECT * FROM car";

        String sql1 = "SELECT * from car EXCEPT SELECT c.id_car, c.name, c.marque, c.annee, c.puissance, c.prix FROM car as c, vente as v where v.id_car_fk = c.id_car";

        try {
            con = Connection.getConnection();

            PreparedStatement stmt = con.prepareStatement(sql);


            ResultSet rs = stmt.executeQuery();


            while (rs.next()) {


                Car c = new Car();

                c.setId(rs.getInt("id_car"));
                c.setNom(rs.getString("nom"));
                c.setMarque(rs.getString("marque"));
                c.setAnnee(rs.getInt("annee"));
                c.setPuissance(rs.getInt("puissance"));
                c.setPrix(rs.getDouble("prix"));

                listeCar.add(c);

            }

            rs.close();

            return listeCar;

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return null;
        } finally {
            Connection.closeConnection(con);
        }
    }

    public void deleteCar(int id) {


        String sql1 = "DELETE FROM car WHERE id_car = ?";
        String sql2 = "DELETE FROM vente WHERE id_car_fk = ?";

        try {
            con = Connection.getConnection();

            PreparedStatement stmt2 = con.prepareStatement(sql2);

            stmt2.setInt(1, id);
            stmt2.executeUpdate();


            PreparedStatement stmt1 = con.prepareStatement(sql1);

            stmt1.setInt(1, id);
            stmt1.executeUpdate();
            System.out.println("\nVoiture supprimée de la base de données\n");
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }

    }


    public Car getCarById(int id) {

        Car c = new Car();
        String sql = "SELECT * FROM car WHERE id_car = ?";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);

            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {

                c.setId(rs.getInt("id_car"));
                c.setNom(rs.getString("nom"));
                c.setMarque(rs.getString("marque"));
                c.setAnnee(rs.getInt("annee"));
                c.setPuissance(rs.getInt("puissance"));
                c.setPrix(rs.getDouble("prix"));
            }
            return c;

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return null;
        } finally {
            Connection.closeConnection(con);
        }
    }


    public void updateCar(int id, String nom, String marque, int annee, int puissance, double prix) {

        String sql = "UPDATE car SET nom = ?, marque = ?, annee = ?, puissance = ?, prix = ? WHERE id_car = ?";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, nom);
            stmt.setString(2, marque);
            stmt.setInt(3, annee);
            stmt.setInt(4, puissance);
            stmt.setDouble(5, prix);
            stmt.setInt(6, id);
            stmt.executeUpdate();
            System.out.println("\nVoiture modifiée dans la base de données\n");
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }

}
