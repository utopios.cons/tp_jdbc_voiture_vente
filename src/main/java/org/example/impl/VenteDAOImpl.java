/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.impl;


import org.example.dao.VenteDao;
import org.example.entity.Vente;
import org.example.utils.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class VenteDAOImpl implements VenteDao {

    private java.sql.Connection con = null;

    public VenteDAOImpl() {
        con = Connection.getConnection();
    }

    public void addVente(Vente v) {
        String sql = "INSERT INTO vente (id_vente, date_vente, id_person_fk, id_car_fk) VALUES (DEFAULT, ?, ?, ?)";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, v.getDate_vente());
            stmt.setInt(2, v.getP().getId());
            stmt.setInt(3, v.getC().getId());
            stmt.execute();
            System.out.println("\nVente ajoutée dans la base de données\n");

        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }


    public void showVente() {

        String sql = "SELECT v.id_vente, p.id_person || ' - ' || p.name as ACHETEUR, "
                + "c.id_car || ' - ' || c.nom as car, c.prix, v.date_vente  FROM vente as v, "
                + "person as p, car as c where p.id_person = v.id_person_fk and c.id_car = v.id_car_fk";

        try {
            con = Connection.getConnection();
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            System.out.println("\nToutes ventes\n");

            while (rs.next()) {
                System.out.println("Numero de la vente: " + rs.getInt("id_vente"));
                System.out.println("Acheteur: " + rs.getString("ACHETEUR"));
                System.out.println("Car: " + rs.getString("car"));
                System.out.println("Prix: " + rs.getDouble("prix"));
                System.out.println("Date de la vente: " + rs.getString("date_vente"));
                System.out.print("\n");
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        } finally {
            Connection.closeConnection(con);
        }
    }

}
